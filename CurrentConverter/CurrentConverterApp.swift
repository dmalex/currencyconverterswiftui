//
//  CurrentConverterApp.swift
//  CurrentConverter
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

@main
struct CurrentConverterApp: App
{
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
